import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:decor_photos/models/models.dart';
import 'package:equatable/equatable.dart';

part 'page_event.dart';
part 'page_state.dart';

class PageBloc extends Bloc<PageEvent, PageState> {
  @override
  PageState get initialState => OnInitialPage();

  @override
  Stream<PageState> mapEventToState(
    PageEvent event,
  ) async* {
    if (event is GotoSplashPage) {
      yield OnSplashPage();
    } else if (event is GotoLoginPage) {
      yield OnLoginPage();
    } else if (event is GotoMainPage) {
      yield OnMainPage();
    } else if (event is GotoRegistrationPage) {
      yield OnRegistrationPage(event.registrationData);
    } else if (event is GotoPreferencePage) {
      yield OnPreferencePage(event.registrationData);
    } else if (event is GotoAccountConfirmationPage) {
      yield OnAccountConfirmationPage(event.registrationData); }
  }
}
