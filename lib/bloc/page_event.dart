part of 'page_bloc.dart';

abstract class PageEvent extends Equatable {
  const PageEvent();
}

class GotoSplashPage extends PageEvent {
  @override
  List<Object> get props => [];
}

class GotoLoginPage extends PageEvent {
  @override
  List<Object> get props => [];
}

class GotoMainPage extends PageEvent {
  @override
  List<Object> get props => [];
}

class GotoRegistrationPage extends PageEvent {
  final RegistrationData registrationData;
  GotoRegistrationPage(this.registrationData);
  @override
  List<Object> get props => [];
}

class GotoPreferencePage extends PageEvent {
  final RegistrationData registrationData;
  GotoPreferencePage(this.registrationData);
  @override
  List<Object> get props => [];
}

class GotoAccountConfirmationPage extends PageEvent {
  final RegistrationData registrationData;
  GotoAccountConfirmationPage(this.registrationData);
  @override
  List<Object> get props => [];
}
